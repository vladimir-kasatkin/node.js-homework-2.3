const express = require("express");
const app = express();
var bodyParser = require("body-parser");

var urlencodedParser = bodyParser.urlencoded({extended: false});

/*

GET / - Главная страница которая вернет код 200 OK и покажет текст "Hello Express.js"
GET /hello - Страница, код ответа 200 OK и покажет текст "Hello stranger !"
GET /hello/[любое имя] - Страница, код ответа 200 OK и покажет текст "Hello, [любое имя] !"
ANY /sub/[что угодно]/[возможно даже так] - Любая из этих страниц должна показать текст "You requested URI: [полный URI запроса]"
POST /post - Страница которая вернет все тело POST запроса (POST body) в JSON формате, либо 404 Not Found - если нет тела запроса

*/


app.get('/', function(req, res) {
  res.status(200).send("Hello Express.js");
});

app.get('/hello', function(req, res) {
  res.status(200).send("Hello stranger !");
});

app.get('/hello/:name', function(req, res) {
  res.status(200).send("Hello, " + req.params.name);
});

// app.all("/sub/:any1/any2", function(req, res) {
app.all(/\/sub\/[^\/.]+\/[^\/.]*$/, function(req, res) {
  res.status(200).send("You requested URI: " + req.protocol + "://" + req.hostname + ":1337"+req.originalUrl);
});

app.post('/post', urlencodedParser, function(req, res) {
  if (req.body) {
    res.json(req.body);
  } else {
    res.status(404).send("Not Found");
  }
});

// обработка 404
app.use(function(req, res, next){
    res.status(404);
    res.send(' no rule ');
    return;
});

// обработка ошибок
app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('500 Something broke!');
});


app.listen(1337);
